<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DbImageTable</name>
    <message>
        <source>Cant connect to database</source>
        <translation>Не удаётся подключиться к базе данных</translation>
    </message>
    <message>
        <source>Successfully connected to database</source>
        <translation>Успешно подключились к базе данных</translation>
    </message>
    <message>
        <source>Cant open database</source>
        <translation>Не удаётся открыть базу данных</translation>
    </message>
    <message>
        <source>Row not selected</source>
        <translation>Строка не выбрана</translation>
    </message>
</context>
<context>
    <name>DbTable</name>
    <message>
        <source>Cant connect to database</source>
        <translation>Не удаётся подключиться к базе данных</translation>
    </message>
    <message>
        <source>Successfully connected to database</source>
        <translation>Успешно подключились к базе данных</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation>Главное окно</translation>
    </message>
    <message>
        <source>Add new patient</source>
        <translation>Добавить нового пациента</translation>
    </message>
    <message>
        <source>Delete patient</source>
        <translation>Удалить пациента</translation>
    </message>
    <message>
        <source>Discharge patient</source>
        <translation>Выписать пациента</translation>
    </message>
    <message>
        <source>Show patient info</source>
        <translation>Показать информацию о пациенте</translation>
    </message>
    <message>
        <source>List of patients :</source>
        <translation>Список пациентов:</translation>
    </message>
    <message>
        <source>Save changes</source>
        <translation>Сохранить изменения</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <source>Add image</source>
        <translation>Добавить снимок</translation>
    </message>
    <message>
        <source>Delete Image</source>
        <translation>Удалить снимок</translation>
    </message>
    <message>
        <source>ID: </source>
        <translation>Номер: </translation>
    </message>
    <message>
        <source>Full name: </source>
        <translation>ФИО: </translation>
    </message>
    <message>
        <source>Date of birth: </source>
        <translation>Дата рождения: </translation>
    </message>
    <message>
        <source>Address: </source>
        <translation>Адрес: </translation>
    </message>
    <message>
        <source>Receipt date: </source>
        <translation>Дата поступления: </translation>
    </message>
    <message>
        <source>Discharge date: </source>
        <translation>Дата выписки: </translation>
    </message>
    <message>
        <source>d/M/yyyy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images :</source>
        <translation>Снимки:</translation>
    </message>
    <message>
        <source>Patient info:</source>
        <translation>Данные пациента:</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <source>Are you really want to delete this image?</source>
        <translation>Вы действительно хотите удалить этот снимок?</translation>
    </message>
    <message>
        <source>ERROR</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>Enter patient name</source>
        <translation>Введите имя пациента</translation>
    </message>
    <message>
        <source>Enter patient address</source>
        <translation>Введите адрес пациента</translation>
    </message>
    <message>
        <source>Enter patient birth date</source>
        <translation>Ввседите дату рождения пациента</translation>
    </message>
    <message>
        <source>Enter patient receipt date</source>
        <translation>Ввседите дату поступления пациента</translation>
    </message>
    <message>
        <source>The data has been modified.</source>
        <translation>Данные были изменены.</translation>
    </message>
    <message>
        <source>Do you want to save your changes?</source>
        <translation>Ходите сохранить изменения?</translation>
    </message>
    <message>
        <source>This patient was discharged before</source>
        <translation>Этот пациент уже выписан</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this patient?</source>
        <translation>Вы точно хотите удалить этого пациента?</translation>
    </message>
    <message>
        <source>English language is installed</source>
        <translation>Установлен английский язык</translation>
    </message>
    <message>
        <source>Russian language is installed</source>
        <translation>Установлен русский язык</translation>
    </message>
    <message>
        <source>Cant load translation file</source>
        <translation>Не удаётся загрузить файл с переводами</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Стиль</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Светный</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Тёмный</translation>
    </message>
    <message>
        <source>Dark style is installed</source>
        <translation>Установлен темный стиль</translation>
    </message>
    <message>
        <source>Light style is installed</source>
        <translation>Установлен светлый стиль</translation>
    </message>
    <message>
        <source>Hospital patients</source>
        <translation>Пациенты больницы</translation>
    </message>
</context>
</TS>
