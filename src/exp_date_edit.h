#ifndef EXP_DATE_EDIT_H
#define EXP_DATE_EDIT_H

#include <QWidget>
#include <QtWidgets/QDateEdit>
#include <QString>

#define QT_NO_CAST_FROM_ASCII

class ExpDateEdit : public QDateEdit
{
   Q_OBJECT
public:
   explicit ExpDateEdit( QWidget* parent = nullptr );
   //returns true if date is 1/1/1900
   bool isDefaultDate();
   //sets date 1/1/1900
   void setDefaultDate();
   //sets date based on string argument
   void fromString( const QString& _date );
   //returns converted date to string
   QString toString();

signals:

public slots:
};

#endif // EXP_DATE_EDIT_H
