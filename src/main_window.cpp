#include "main_window.h"
#include "ui_main_window.h"

#include <QFile>
#include <QFileDialog>
#include <QStateMachine>
#include <QState>
#include <QSqlRecord>
#include <QTranslator>

MainWindow::MainWindow( QWidget* parent ) :
   QMainWindow( parent ),
   ui( new Ui::MainWindow ),
   translator( new QTranslator( this ) )
{
   ui->setupUi( this );

   setWindowTitle( tr( "Hospital patients" ) );

   if( !translator->load( ":/translations/patients_ru" ) )
      qDebug() << tr( "Cant load translation file" );

   QFile file( ":/styles/styles.css" );
   if( file.open( QIODevice::ReadOnly ) )
   {
      m_styles = file.readAll();
      file.close();
   }
   else
   {
      qDebug() << "Cant open the file \"styles.css\"";
   }

   m_baseStyle = styleSheet();

   setStartView();

   connect( ui->btnAddPatient, &QPushButton::clicked, this, &MainWindow::setNewPatientView );

   connect( ui->btnBack, &QPushButton::clicked, this, &MainWindow::backFromPatientInfo );
   connect( ui->btnShowPatientInfo, &QPushButton::clicked, this, &MainWindow::setExistPatientView );
   connect( ui->tableView, &QAbstractItemView::doubleClicked, this, &MainWindow::setExistPatientView );

   // set buttons disabled if row isn`t selected
   connect( ui->tableView->selectionModel(), &QItemSelectionModel::selectionChanged, this, &MainWindow::handleItemSelection );

   connect( ui->imageView->selectionModel(), &QItemSelectionModel::selectionChanged,  this, &MainWindow::handleImageSelection );

   // save changes of modifying patient data
   connect( ui->btnSaveChanges, &QPushButton::clicked, this, &MainWindow::saveChanges );

   // if patient data was changed ask user if he want to save changes before exit
   connect( ui->FullNameEdit,       &QLineEdit::textChanged, this, &MainWindow::handlePatientDataChanged );
   connect( ui->addressEdit,        &QLineEdit::textChanged, this, &MainWindow::handlePatientDataChanged );
   connect( ui->birthDateEdit,      &QDateEdit::dateChanged, this, &MainWindow::handlePatientDataChanged );
   connect( ui->receiptDateEdit,    &QDateEdit::dateChanged, this, &MainWindow::handlePatientDataChanged );
   connect( ui->dischargeDateEdit,  &QDateEdit::dateChanged, this, &MainWindow::handlePatientDataChanged );
   connect( ui->imageView->model(), &QAbstractItemModel::dataChanged, this, &MainWindow::handlePatientDataChanged );
   connect( ui->imageView->model(), &QAbstractItemModel::rowsRemoved, this, &MainWindow::handlePatientDataChanged );

   // handle 'add' button click
   connect( ui->btnAdd, &QPushButton::clicked, this, &MainWindow::checkAndSave );
   // discharge patient
   connect( ui->btnDischPatient, &QPushButton::clicked, this, &MainWindow::dischargePatient );
   // delete patient
   connect( ui->btnDelPatient, &QPushButton::clicked, this, &MainWindow::deletePatient );

   connect( ui->btnAddImage, &QPushButton::clicked, [&]()
   {
      ui->imageView->addImage( ui->IdEdit->text() );
   });

   connect( ui->btnDeleteImg, &QPushButton::clicked, this, &MainWindow::deletePatientImage);

}

MainWindow::~MainWindow()
{
   delete ui;
}

void MainWindow::fillExistPatientData()
{
   int selectedRow = ui->tableView->currentIndex().row();
   QSqlRecord record = ui->tableView->record( selectedRow );

   QString id            = record.value( "id" ).toString();
   QString name          = record.value( "Patient name" ).toString();
   QString birthDate     = record.value( "Date of birth" ).toString();
   QString address       = record.value( "Address" ).toString();
   QString receiptDate   = record.value( "Receipt date" ).toString();
   QString dischargeDate = record.value( "Discharge date" ).toString();

   ui->IdEdit->setText( id );
   ui->FullNameEdit->setText( name );
   ui->birthDateEdit->fromString( birthDate );
   ui->addressEdit->setText( address );
   ui->receiptDateEdit->fromString( receiptDate );
   ui->dischargeDateEdit->fromString( dischargeDate );

   ui->imageView->selectImages( id );
}

void MainWindow::fillNewPatientData()
{
   // set new user id
   QAbstractItemModel * model = ui->tableView->model();
   int lastRow = model->rowCount()-1;
   int id = model->data( model->index( lastRow, 0 ) ).toInt();
   ui->IdEdit->setText( QString::number( ++id ) );

   ui->imageView->selectImages( QString::number( id ) );
}

void MainWindow::handleItemSelection()
{
   if( ui->tableView->selectionModel()->hasSelection() )
   {
      enableSpacialButtons( true );
   }
   else
   {
      enableSpacialButtons( false );
   }
}

void MainWindow::handleImageSelection()
{
   if( ui->imageView->selectionModel()->hasSelection() )
   {
      ui->btnDeleteImg->setEnabled( true );
      // set picture to image label
      int selectedRow = ui->imageView->currentIndex().row();
      QAbstractItemModel * tableModel =  ui->imageView->model();
      QByteArray baImage = tableModel->data( tableModel->index( selectedRow, 1 ) ).toByteArray();
      QPixmap image;
      image.loadFromData( baImage );
      ui->imageLbl->setScaledPixmap( image );
   }
   else
   {
      ui->btnDeleteImg->setEnabled( false );
   }

}

void MainWindow::deletePatientImage()
{
   QMessageBox msgBox;
   msgBox.setText( tr( "Are you really want to delete this image?" ) );
   msgBox.setIcon(QMessageBox::Question);
   msgBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel );
   msgBox.setDefaultButton( QMessageBox::Yes );

   int ret = msgBox.exec();
   switch ( ret )
   {
      case QMessageBox::Yes:
      {
         ui->imageView->deleteImage();
         ui->btnDeleteImg->setEnabled(false);

      }
      case QMessageBox::No:
      case QMessageBox::Cancel:
      default: break;
   }
}

void MainWindow::clearPatientData()
{
   ui->IdEdit->clear();
   ui->FullNameEdit->clear();
   ui->birthDateEdit->setDefaultDate();
   ui->addressEdit->clear();
   ui->receiptDateEdit->setDefaultDate();
   ui->dischargeDateEdit->setDefaultDate();

   ui->imageLbl->setDefault();
}

bool MainWindow::checkAndSave()
{
   QString name = ui->FullNameEdit->text();
   QString address = ui->addressEdit->text();

   if( name.isEmpty() )
   {
      QMessageBox::critical( this, tr( "ERROR" ), tr( "Enter patient name" ) );
      return false;
   }
   if( address.isEmpty() )
   {
      QMessageBox::critical( this, tr( "ERROR" ), tr( "Enter patient address" ) );
      return false;
   }
   if( ui->birthDateEdit->isDefaultDate() )
   {
      QMessageBox::critical( this, tr( "ERROR" ), tr( "Enter patient birth date" ) );
      return false;
   }
   if( ui->receiptDateEdit->isDefaultDate() )
   {
      QMessageBox::critical( this, tr( "ERROR" ), tr( "Enter patient receipt date" ) );
      return false;
   }

   QString id = ui->IdEdit->text();
   QString birthDate = ui->birthDateEdit->toString();
   QString receiptDate = ui->receiptDateEdit->toString();

   ui->tableView->addNewPatient( id, name, birthDate, address, receiptDate );
   ui->imageView->saveChanges();
   setStartView();
   return true;
}

void MainWindow::enableSpacialButtons( bool _enable )
{
   ui->btnShowPatientInfo->setEnabled( _enable );
   ui->btnDelPatient->setEnabled( _enable );
   ui->btnDischPatient->setEnabled( _enable );
}

void MainWindow::changeEvent( QEvent* event )
{
   if( event->type() == QEvent::LanguageChange )
      ui->retranslateUi(this);
}

bool MainWindow::isExistPatientView()
{
   return ui->btnAdd->isHidden();
}

bool MainWindow::isNewPatientView()
{
   return !ui->btnAdd->isHidden();
}

void MainWindow::backFromPatientInfo()
{
   if( m_isPatientDataChanged )
   {
      QMessageBox msgBox;
      msgBox.setText( tr( "The data has been modified." ) );
      msgBox.setInformativeText( tr( "Do you want to save your changes?" ) );
      msgBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel );
      msgBox.setDefaultButton( QMessageBox::Yes );

      int ret = msgBox.exec();
      switch ( ret )
      {
         case QMessageBox::Yes:
         {
            if( isNewPatientView() && checkAndSave() )
            {
               setStartView();
               return;
            }

            if( isExistPatientView() )
            {
               saveChanges();
               setStartView();
               return;
            }
            break;
         }
         case QMessageBox::No:
         {
            ui->imageView->revertChanges();
            setStartView();
            return;
         }
         case QMessageBox::Cancel:
         default: return;
      }
   }
   else
   {
      ui->imageView->revertChanges();
      setStartView();
   }
}

void MainWindow::dischargePatient()
{
   if( ui->tableView->currentIndex().row() == -1 ) return;

   int selectedRow = ui->tableView->currentIndex().row();
   QAbstractItemModel * tableModel =  ui->tableView->model();
   QString dischargeDate = tableModel->data( tableModel->index( selectedRow, 5 ) ).toString();

   if( dischargeDate.contains( '-' ) )
      ui->tableView->dischargePatient();
   else
      QMessageBox::critical( this, tr( "ERROR" ), tr( "This patient was discharged before" ) );
}

void MainWindow::deletePatient()
{
   int selectedRow = ui->tableView->currentIndex().row();
   if( selectedRow == -1 ) return;

   QMessageBox msgBox;
   msgBox.setText( tr( "Are you sure you want to delete this patient?" ) );
   msgBox.setIcon( QMessageBox::Question );
   msgBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel );
   msgBox.setDefaultButton( QMessageBox::No );
   int ret = msgBox.exec();
   switch ( ret )
   {
      case QMessageBox::Yes:
      {
         ui->tableView->deletePatient( selectedRow );

         QAbstractItemModel * model = ui->imageView->model();
         QString id = model->data( model->index( selectedRow, 0 ) ).toString();
         ui->imageView->deletePatientImages( id );
         ui->tableView->updateTable();
         setStartView();
         break;
      }
      case QMessageBox::No:
      case QMessageBox::Cancel:
      default:
         break;
   }
}

void MainWindow::saveChanges()
{
   ui->tableView->editPatient(
            ui->FullNameEdit->text(),
            ui->birthDateEdit->toString(),
            ui->addressEdit->text(),
            ui->receiptDateEdit->toString(),
            ui->dischargeDateEdit->toString() );
   ui->imageView->saveChanges();
   setExistPatientView();
}


void MainWindow::setStartView()
{
   ui->stackedWidget->setCurrentIndex(0);
   enableSpacialButtons(false);
   m_isPatientDataChanged = false;
   ui->tableView->selectionModel()->clearSelection();
}

void MainWindow::setNewPatientView()
{
   ui->stackedWidget->setCurrentIndex( 1 );
   ui->btnAdd->show();
   ui->btnSaveChanges->hide();
   ui->lblDischargeDate->hide();
   ui->dischargeWidget->hide();

   ui->imageLbl->setDefault();
   ui->btnDeleteImg->setEnabled( false );

   clearPatientData();
   fillNewPatientData();
   m_isPatientDataChanged = false;
}

void MainWindow::setExistPatientView()
{
   ui->stackedWidget->setCurrentIndex( 1 );
   ui->imageLbl->setDefault();
   ui->btnAdd->hide();
   ui->btnSaveChanges->show();
   ui->lblDischargeDate->show();
   ui->dischargeWidget->show();

   ui->btnDeleteImg->setEnabled( false );
   fillExistPatientData();
   m_isPatientDataChanged = false;
   ui->btnSaveChanges->setEnabled( false );
}

void MainWindow::handlePatientDataChanged()
{
   if( ui->stackedWidget->currentIndex() == 1 )
   {
      m_isPatientDataChanged = true;
      if( isExistPatientView() )
         ui->btnSaveChanges->setEnabled( true );
   }
}


void MainWindow::on_actionEnglish_toggled( bool arg1 )
{
   if( arg1 && ui->actionRussian->isChecked() )
   {
      qDebug() << tr( "English language is installed" );
      qApp->removeTranslator( translator );
      ui->actionRussian->setChecked(false);
   }
   if( !arg1 && !ui->actionRussian->isChecked() )
   {
      ui->actionEnglish->setChecked(true);
   }
}

void MainWindow::on_actionRussian_toggled( bool arg1 )
{
    if( arg1 && ui->actionEnglish->isChecked() )
    {
       qDebug() << tr( "Russian language is installed" );
       qApp->installTranslator( translator );
       ui->actionEnglish->setChecked( false );
    }
    if( !arg1 && !ui->actionEnglish->isChecked() )
    {
       ui->actionRussian->setChecked( true );
    }
}

void MainWindow::on_actionDark_toggled( bool arg1 )
{
   if( arg1 && ui->actionLight->isChecked() )
   {
      qDebug() << tr( "Dark style is installed" );
      setStyleSheet(m_styles);
      ui->actionLight->setChecked( false );
   }
   if( !arg1 && !ui->actionLight->isChecked() )
   {
      ui->actionDark->setChecked( true );
   }
}

void MainWindow::on_actionLight_toggled( bool arg1 )
{
   if( arg1 && ui->actionDark->isChecked() )
   {
      qDebug() << tr( "Light style is installed" );
      setStyleSheet(m_baseStyle);
      ui->actionDark->setChecked( false );
   }
   if( !arg1 && !ui->actionDark->isChecked() )
   {
      ui->actionLight->setChecked( true );
   }
}
