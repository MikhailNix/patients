#include "db_image_table.h"

#include <QBuffer>
#include <QDate>
#include <QDebug>
#include <QDir>
#include <QHeaderView>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlRelationalTableModel>
#include <QtSql/QSqlRecord>
#include <QtWidgets/QFileDialog>
#include <QString>
#include <QSqlError>
#include <QMessageBox>

DbImageTable::DbImageTable( QWidget* parent ) : QTableView( parent )
{
   m_database = QSqlDatabase::addDatabase( "QSQLITE", "image" );

   //configure path to database
   QString dbPath = QDir::currentPath();
   int i = dbPath.indexOf("bin");
   dbPath.remove(i, dbPath.length()-i);
   dbPath+="resources/patients.db";

   //open database and read all data
   m_database.setDatabaseName( dbPath );
   if( !m_database.isOpen() ) m_database.open();

   if( !m_database.isOpen() )
   {
      qDebug() << tr( "Cant connect to database" );
   }
   else
   {
      qDebug() << tr( "Successfully connected to database" );

      m_pTableModel = new QSqlRelationalTableModel( this, m_database );
      m_pTableModel->setTable( "images" );
     // m_pTableModel->setRelation( 2, QSqlRelation( "patients", "Patient_Id" ,"Patient_Id" ) );
      setModel( m_pTableModel );
      m_pTableModel->setEditStrategy( QSqlTableModel::OnManualSubmit );
      setSelectionBehavior( QAbstractItemView::SelectRows );
      setSelectionMode( QAbstractItemView::SingleSelection );
      horizontalHeader()->setStretchLastSection( true );
      hideColumn( 0 );
      hideColumn( 1 );
      hideColumn( 2 );
   }
}

bool DbImageTable::selectImages( const QString& _patientId )
{
   if(!m_database.isOpen())
   {
      qDebug() << tr("Cant open database");
      return false;
   }

   m_pTableModel->setFilter( "Patient_Id='"+_patientId+"'" );
   qDebug() << m_pTableModel->filter();
   m_pTableModel->select();
   return true;
}

void DbImageTable::addImage( const QString& _patientId )
{
   QString imagePath( QFileDialog::getOpenFileName( this ) );

   if( imagePath.isEmpty() )
   {
      qDebug() << "Path to image is empty";
      return;
   }

   QPixmap image( imagePath );
   if( image.isNull() )
   {
      qDebug() << "Cant get an image";
      return;
   }

   QByteArray inByteArray;
   QBuffer inBuffer( &inByteArray );
   inBuffer.open( QIODevice::WriteOnly );
   image.save( &inBuffer, "JPG" );

   if( !m_database.isOpen() )
   {
      qDebug() << "Cant open database";
      return;
   }

   // add new row
   model()->insertRow( model()->rowCount() );
   // fill it with data
   int row = model()->rowCount()-1;

   model()->setData( model()->index( row, 1 ), inByteArray );
   model()->setData( model()->index( row, 2 ), _patientId );
   model()->setData( model()->index( row, 3 ), QDate::currentDate().toString( "d/M/yyyy" ) );
   model()->setData( model()->index( row, 4 ), "Image" );
}

void DbImageTable::saveChanges()
{
   m_pTableModel->submitAll();
}

void DbImageTable::revertChanges()
{
   m_pTableModel->revertAll();
}

bool DbImageTable::deleteImage( int _selectedRow )
{
   if( !m_database.isOpen() )
   {
      qDebug() << tr("Cant connect to database");
      return false;
   }

   model()->removeRow( _selectedRow );
   m_pTableModel->submitAll();
   return true;
}

void DbImageTable::deletePatientImages( const QString& _patientId )
{
   m_pTableModel->query().prepare( "DELETE FROM images WHERE Patient_Id='"+_patientId+"'" );
   m_pTableModel->query().exec();
   m_pTableModel->submitAll();
}

bool DbImageTable::deleteImage()
{
   if( !m_database.isOpen() )
   {
      qDebug() << tr("Cant connect to database");
      return false;
   }

   int selectedRow = currentIndex().row();
   if (selectedRow < 0)
   {
      qDebug() << tr("Row not selected");
      return false;
   }

   model()->removeRow( selectedRow );
   saveChanges();
   return true;
}
