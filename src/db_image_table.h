#ifndef DB_IMAGE_TABLE_H
#define DB_IMAGE_TABLE_H

#include <QtSql/QSqlDatabase>
#include <QtWidgets/QTableView>

class QSqlTableModel;
class QSqlRelationalTableModel;

class DbImageTable : public QTableView
{
   Q_OBJECT
public:
   explicit DbImageTable( QWidget* parent = nullptr );
   bool selectImages( const QString& _patientId );
   void addImage( const QString& _patientId );
   void saveChanges();
   void revertChanges();
   bool deleteImage( int _selectedRow );
   void deletePatientImages( const QString& _patientId);
public slots:
   bool deleteImage();
private:
    QSqlDatabase m_database;
    QSqlRelationalTableModel* m_pTableModel;
};

#endif // DB_IMAGE_TABLE_H
