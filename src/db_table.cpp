#include "db_table.h"

#include <QDate>
#include <QDebug>
#include <QDir>
#include <QHeaderView>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlRecord>
#include <QStandardPaths>
#include <QString>
#include <QSqlError>
#include <QMessageBox>

DbTable::DbTable( QWidget* parent) : QTableView( parent )
{
   m_database = QSqlDatabase::addDatabase( "QSQLITE", "patient" );

   //configure path to database
   QString dbPath = QDir::currentPath();
   int i = dbPath.indexOf("bin");
   dbPath.remove(i, dbPath.length()-i);
   dbPath+="resources/patients.db";

   //open database and read all data
   m_database.setDatabaseName( dbPath );
   if( !m_database.isOpen() ) m_database.open();

   if( !m_database.isOpen() )
   {
      qDebug() << tr( "Cant connect to database" );
   }
   else
   {
      qDebug() << tr( "Successfully connected to database" );

      m_pTableModel = new QSqlTableModel( this, m_database );
      m_pTableModel->setTable( "patients" );
      m_pTableModel->select();
      this->setModel( m_pTableModel );
      this->setSelectionMode( QAbstractItemView::SingleSelection );
      this->horizontalHeader()->setStretchLastSection( true );
   }
}

bool DbTable::editPatient(
      const QString& _name,
      const QString& _birthDate,
      const QString& _address,
      const QString& _receiptDate,
      const QString& _dischargeDate)
{

   if( !m_database.isOpen() )
   {
      qDebug() << tr( "Cant connect to database" );
      return false;
   }

   int selectedRow = this->currentIndex().row();
   QSqlRecord record = m_pTableModel->record(selectedRow);

   record.setValue("Patient name", _name);
   record.setValue("Date of birth", _birthDate);
   record.setValue("Address", _address);
   record.setValue("Receipt date", _receiptDate);
   record.setValue("Discharge date", _dischargeDate);

   m_pTableModel->setRecord(selectedRow, record);

   m_pTableModel->submitAll();
   return true;
}

bool DbTable::addNewPatient(
      const QString& _id,
      const QString& _name,
      const QString& _birthDate,
      const QString& _address,
      const QString& _receiptDate )
{
   if( !m_database.isOpen() )
   {
      qDebug() << tr("Cant connect to database");
      return false;
   }
  // add new row
  model()->insertRow( model()->rowCount() );

  // fill it with data
  int row = model()->rowCount()-1;
  model()->setData( model()->index( row,0 ), _id );
  model()->setData( model()->index( row,1 ), _name );
  model()->setData( model()->index( row,2 ), _birthDate );
  model()->setData( model()->index( row,3 ), _address );
  model()->setData( model()->index( row,4 ), _receiptDate );

  m_pTableModel->submitAll();
  return true;
}

bool DbTable::dischargePatient()
{
   if( !m_database.isOpen() )
   {
      qDebug() << tr("Cant connect to database");
      return false;
   }

   QDate today = QDate::currentDate();
   int row = currentIndex().row();
   model()->setData( model()->index( row, 5 ), today.toString( "d/M/yyyy" ) );
   m_pTableModel->submitAll();
   return true;
}

bool DbTable::deletePatient( int _selectedRow )
{
   if( !m_database.isOpen() )
   {
      qDebug() << tr("Cant connect to database");
      return false;
   }

   model()->removeRow( _selectedRow );
   m_pTableModel->submitAll();
   return true;
}

void DbTable::updateTable()
{
   m_pTableModel->select();
}

QSqlRecord DbTable::record( int _row )
{
   return m_pTableModel->record( _row );
}
