#ifndef IMAGE_LABEL_H
#define IMAGE_LABEL_H

#include <QtWidgets/QLabel>

class QPixmap;

class ImageLabel : public QLabel
{
   Q_OBJECT
public:
   explicit ImageLabel( QWidget* parent = nullptr );
   ~ImageLabel();

   void setScaledPixmap( const QPixmap& _pixmap );
   void setDefault();
protected:
    void paintEvent( QPaintEvent* );

private:
   QPixmap m_pixmap;
};

#endif // IMAGE_LABEL_H
