#ifndef DB_TABLE_H
#define DB_TABLE_H

#include <QtSql/QSqlDatabase>
#include <QtWidgets/QTableView>

class QSqlTableModel;

class DbTable : public QTableView
{
   Q_OBJECT
public:
   explicit DbTable( QWidget* parent = nullptr );
   //writes to database new parameters for a patient
   bool editPatient(
                    const QString& _name,
                    const QString& _birthDate,
                    const QString& _address,
                    const QString& _receiptDate,
                    const QString& _dischargeDate );
   //writes new patient to the database
   bool addNewPatient(
                    const QString& _id,
                    const QString& _name,
                    const QString& _birthDate,
                    const QString& _address,
                    const QString& _receiptDate );
   // makes patient in database discharged
   bool dischargePatient();
   // deletes patient from database
   bool deletePatient( int _selectedRow );
   void updateTable();
   QSqlRecord record( int _row );

private:
    QSqlDatabase m_database;
    QSqlTableModel* m_pTableModel;
};

#endif // DB_TABLE_H
