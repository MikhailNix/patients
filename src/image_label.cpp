#include "image_label.h"

#include <QPainter>
#include <QPixmap>

ImageLabel::ImageLabel( QWidget *parent ) : QLabel( parent )
{
}

ImageLabel::~ImageLabel()
{
}

void ImageLabel::setScaledPixmap( const QPixmap& _pixmap )
{
   m_pixmap=_pixmap;
   update();
}

void ImageLabel::setDefault()
{
   QPixmap pixmap;
   m_pixmap = pixmap;
   update();
}

void ImageLabel::paintEvent( QPaintEvent* const _event )
{
   QPainter painter( this );

   if( !m_pixmap.isNull() )
   {
       QPoint centerPoint( 0,0 );

       QSize scaledSize = size();

       QPixmap scaledPixmap = m_pixmap.scaled( scaledSize, Qt::KeepAspectRatio );

       centerPoint.setX( ( scaledSize.width()-scaledPixmap.width() )/2 );
       centerPoint.setY( ( scaledSize.height()-scaledPixmap.height() )/2 );

       painter.drawPixmap( centerPoint, scaledPixmap );
   }

   QLabel::paintEvent( _event );
}

