#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlTableModel>
#include <QDebug>
#include <QString>
#include <QMessageBox>

class QTranslator;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   explicit MainWindow( QWidget* parent = 0 );
   ~MainWindow();

public slots:
   void fillExistPatientData();
   void clearPatientData();
   bool checkAndSave();
   void enableSpacialButtons( bool );

protected:
   void changeEvent( QEvent* event ) override;

private:
   Ui::MainWindow* ui;
   QTranslator* translator;
   bool m_isPatientDataChanged;

   QString m_styles;
   QString m_baseStyle;

   bool isExistPatientView();
   bool isNewPatientView();

private slots:
   void fillNewPatientData();
   void backFromPatientInfo();
   void dischargePatient();
   void deletePatient();
   void saveChanges();
   void handleItemSelection();
   void handleImageSelection();
   void deletePatientImage();

   void setStartView();
   void setNewPatientView();
   void setExistPatientView();

   void handlePatientDataChanged();

   void on_actionEnglish_toggled(bool arg1);
   void on_actionRussian_toggled(bool arg1);
   void on_actionDark_toggled(bool arg1);
   void on_actionLight_toggled(bool arg1);
};

#endif // MAINWINDOW_H
