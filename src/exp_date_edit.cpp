#include "exp_date_edit.h"

ExpDateEdit::ExpDateEdit( QWidget* parent ) : QDateEdit( parent )
{
}

bool ExpDateEdit::isDefaultDate()
{
   if( date().day() == 1 && date().month() == 1 && date().year() == 1900 )
      return true;
   else
      return false;
}

void ExpDateEdit::setDefaultDate()
{
   QDate newDate( 1900, 1, 1 );
   this->setDate( newDate );
}

void ExpDateEdit::fromString(const QString& _date)
{
   // strange behaviour  - always returns true
   // bool isDashes = (_date == tr("--/--/----"));
   bool isDashes = _date.contains( '-' );
   if( isDashes )
   {
      setDefaultDate();
      return;
   }

   int day;
   int month;
   int year;

   QStringList dateList = _date.split( "/" );

   day = dateList[0].toInt();
   month = dateList[1].toInt();
   year = dateList[2].toInt();

   QDate newDate( year, month, day );
   this->setDate( newDate );
}

QString ExpDateEdit::toString()
{
   QString result;
   if( isDefaultDate() )
   {
      result = "--/--/----";
   }
   else
   {
      result += QString::number( date().day() );
      result += '/';
      result += QString::number( date().month() );
      result += '/';
      result += QString::number (date().year() );
   }
   return result;
}
