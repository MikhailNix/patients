#-------------------------------------------------
QT += gui core widgets sql

#CONFIG += console c++11
#CONFIG -= debug_and_release

TARGET = "Patients"
TEMPLATE = app

DEFINES += PRO_FILE_PWD=$$sprintf("\"\\\"%1\\\"\"", $$_PRO_FILE_PWD_)

INCLUDEPATH += \
    ../src

HEADERS += \
    ../src/main_window.h \
    ../src/db_table.h \
    ../src/exp_date_edit.h \
    ../src/db_image_table.h \
    ../src/image_label.h

SOURCES += \
    ../src/main.cpp \
    ../src/main_window.cpp \
    ../src/db_table.cpp \
    ../src/exp_date_edit.cpp \
    ../src/db_image_table.cpp \
    ../src/image_label.cpp

FORMS += \
    ../src/main_window.ui

TRANSLATIONS += \
    ../resources/patients_ru.ts

CONFIG( debug, debug|release ) {
    # debug
    BUILD_CONFIG = "debug"
}

CONFIG( release, debug|release ) {
    # release
    BUILD_CONFIG = "release"
}

QMAKE_TARGET_DESCRIPTION = "Patients for Qt .ts files"
QMAKE_TARGET_COMPANY = "Nix Solutions Ltd."
QMAKE_TARGET_PRODUCT = "Patients"
QMAKE_TARGET_COPYRIGHT = "Copyright \\251 2018 All rights reserved."
RC_ICONS = ../resources/icon.ico
VERSION = "1.0.0"

DESTDIR = ../bin/$${PLATFORM}_$${QMAKE_HOST.arch}_$${BUILD_CONFIG}
OBJECTS_DIR = $$OUT_PWD/$$BUILD_CONFIG/obj
UI_DIR = $$OUT_PWD/$$BUILD_CONFIG/ui
MOC_DIR = $$OUT_PWD/$$BUILD_CONFIG/moc
RCC_DIR = $$OUT_PWD/$$BUILD_CONFIG/qrc

system("lupdate -no-obsolete -locations none $$shell_path($$PWD/patients.pro)" & \
                               lrelease $$TRANSLATIONS)

RESOURCES += \
    ../resources/rec.qrc
